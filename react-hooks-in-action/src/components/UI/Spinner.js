import { FaSpinner } from "react-icons/all";

export const Spinner = (props) => {
    return (
        <span {...props}>
            <FaSpinner className='icon-loading' />
        </span>
    );
};
