export const ActionTypes = Object.freeze({
    'SET_GROUP': 1,
    'SET_BOOKABLE': 2,
    'NEXT_BOOKABLE': 4,
    'FETCH_BOOKABLES_REQUEST': 5,
    'FETCH_BOOKABLES_SUCCESS': 6,
    'FETCH_BOOKABLES_ERROR': 7,
});

export const reducer = (state, action) => {
    switch (action.type) {
        case ActionTypes.SET_GROUP:
            return {
                ...state,
                group: action.payload,
                selectedBookableId: state.bookables.filter(b => b.group === action.payload)[0].id,
            };
        case ActionTypes.SET_BOOKABLE:
            return {
                ...state,
                selectedBookableId: action.payload,
            };
        case ActionTypes.NEXT_BOOKABLE:
            if (state.bookables.length === 0) {
                return state;
            }
            const bookablesByGroup = state.bookables.filter(b => b.group === state.group);
            const bookable = bookablesByGroup.find(b => b.id === state.selectedBookableId + 1);
            return {
                ...state,
                selectedBookableId: bookable ? bookable.id : bookablesByGroup[0].id,
            };
        case ActionTypes.FETCH_BOOKABLES_REQUEST:
            return {
                ...state,
                isLoading: true,
                error: false,
                bookables: [],
            };
        case ActionTypes.FETCH_BOOKABLES_SUCCESS:
            return {
                ...state,
                isLoading: false,
                bookables: action.payload,
            };
        case ActionTypes.FETCH_BOOKABLES_ERROR:
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            };
        default:
            return state;
    }
};
