import { useEffect, useRef, useState } from "react";
import { FaArrowRight } from "react-icons/all";
import { getData } from "../../utils/api";
import { Spinner } from "../UI/Spinner";

export const BookablesList = ({ bookable, setBookable }) => {
    const [ bookables, setBookables ] = useState([]);
    const [ error, setError ] = useState(false);
    const [ isLoading, setIsLoading ] = useState(true);

    const group = bookable?.group;

    const bookablesInGroup = bookables.filter(b => b.group === group);
    const groups = [ ...new Set(bookables.map(b => b.group)) ];
    const nextButtonRef = useRef();

    useEffect(() => {
        const loadData = async () => {
            try {
                const bookables = await getData('http://localhost:3001/bookables');
                setBookable(bookables[0]);
                setBookables(bookables);
            } catch (error) {
                setError(error);
            }
            setIsLoading(false);
        };
        loadData();
    }, [setBookable]);

    const changeGroup = (e) => {
        const bookablesInSelectedGroup = bookables.filter(b => b.group === e.target.value);
        setBookable(bookablesInSelectedGroup[0]);
    };

    const changeBookable = (selectedBookable) => {
        setBookable(selectedBookable);
        nextButtonRef.current.focus();
    };

    const nextBookable = () => {
        const i = bookablesInGroup.indexOf(bookable);
        const nextIndex = (i + 1) % bookablesInGroup.length;
        const nextBookable = bookablesInGroup[nextIndex];
        setBookable(nextBookable);
    };

    if (error) {
        return <p>{error.message}</p>
    }

    if (isLoading) {
        return <p><Spinner /> Loading bookables...</p>
    }

    return (
        <>
            <div>
                <select
                    value={group}
                    onChange={(e) => changeGroup(e)}
                >
                    {groups.map(g => (
                        <option key={g} value={g}>{g}</option>
                    ))}
                </select>
                <ul className="bookables items-list-nav">
                    {bookablesInGroup.map(b => (
                        <li
                            key={b.id}
                            className={b.id === bookable.id ? "selected" : null}
                        >
                            <button
                                className='btn'
                                onClick={() => changeBookable(b)}>
                                {b.title}
                            </button>
                        </li>
                    ))}
                </ul>
                <p>
                    <button
                        className='btn'
                        autoFocus
                        ref={nextButtonRef}
                        onClick={nextBookable}
                    >
                        <FaArrowRight/>
                        <span>Next</span>
                    </button>
                </p>
            </div>
        </>
    );
}
