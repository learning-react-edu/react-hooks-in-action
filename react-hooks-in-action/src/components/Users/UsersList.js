import { useEffect, useState } from "react";
import { FaArrowRight } from "react-icons/all";
import { getData } from "../../utils/api";
import { Spinner } from "../UI/Spinner";

export const UsersList = ({ user, setUser }) => {
    const [ users, setUsers ] = useState([]);
    const [ isLoading, setIsLoading ] = useState(true);
    const [ error, setError ] = useState(null);

    useEffect(() => {
        const loadUsers = async () => {
            try {
                const users = await getData('http://localhost:3001/users');
                setUser(users[0]);
                setUsers(users);
            } catch (e) {
                setError(e);
            }
            setIsLoading(false);
        }

        loadUsers();
    }, [ setUser ]);

    const nextUser = () => {
        const newUser = users.find(u => u.id === user.id + 1);
        setUser(newUser ? newUser : users[0]);
    };

    if (error) {
        return <p>{error.message}</p>
    }

    if (isLoading) {
        return <p><Spinner/> Loading users...</p>
    }

    return (
        <div>
            <ul className='users  items-list-nav'>
                {users.map(u => (
                    <li
                        key={u.id}
                        className={u.id === user.id ? "selected" : null}
                    >
                        <button
                            className='btn'
                            onClick={() => setUser(u)}
                        >{u.name}</button>
                    </li>
                ))}
            </ul>
            <p>
                <button
                    className='btn'
                    autoFocus
                    onClick={nextUser}
                >
                    <FaArrowRight/>
                    <span>Next</span>
                </button>
            </p>
        </div>
    );
};
