import { useEffect, useState } from "react";
import { Spinner } from "../UI/Spinner";

export const UserPicker = () => {
    const [ users, setUsers ] = useState([]);

    useEffect(() => {
        const loadUsers = async () => {
            const response = await fetch('http://localhost:3001/users');
            const users = await response.json();
            setUsers(users);
        }
        loadUsers();
    }, []);

    if (users === []) {
        return <Spinner/>;
    }

    return (
        <select>
            {users.map(user => (
                <option key={user.id}>{user.name}</option>
            ))}
        </select>
    );
};
