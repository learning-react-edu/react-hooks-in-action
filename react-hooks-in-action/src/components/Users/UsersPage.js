import { UsersList } from "./UsersList";
import { UserDetails } from "./UserDetails";
import { useState } from "react";

export const UsersPage = () => {
    const [ user, setUser ] = useState(null);

    return (
        <main className="users-page">
            <UsersList user={user} setUser={setUser} />
            <UserDetails user={user} />
        </main>
    );
};
