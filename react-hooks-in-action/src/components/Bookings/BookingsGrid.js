export const BookingsGrid = ({ week, bookable, booking, setBooking }) => {
    return (
        <div className='bookingsGrid placeholder'>
            <h3>Bookings Grid</h3>
            <p>{bookable?.title}</p>
            <p>{week.date.toISOString()}</p>
        </div>
    );
};
