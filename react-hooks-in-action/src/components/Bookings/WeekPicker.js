import { useRef } from "react";
import { FaCalendarCheck, FaCalendarDay, FaChevronLeft, FaChevronRight } from "react-icons/all";
import { ActionTypes } from "./weekReducer";

export const WeekPicker = ({ dispatch }) => {
    const textBoxRef = useRef();

    const goToDate = () => {
        dispatch({
            type: ActionTypes.SET_DATE,
            payload: textBoxRef.current.value,
        });
    }

    return (
        <div>
            <p className='date-picker'>
                <button
                    className='btn'
                    onClick={() => dispatch({ type: ActionTypes.PREV_WEEK })}
                >
                    <FaChevronLeft />
                    <span>Perv</span>
                </button>

                <button
                    className='btn'
                    onClick={() => dispatch({ type: ActionTypes.TODAY })}
                >
                    <FaCalendarDay />
                    <span>Today</span>
                </button>
                <span>
                    <input
                        type='text'
                        ref={textBoxRef}
                        placeholder='e.g. 2020-09-02'
                        defaultValue='2021-08-01'
                    />
                </span>
                <button className='go btn' onClick={goToDate}>
                    <FaCalendarCheck />
                    <span>Go</span>
                </button>
                <button
                    className='btn'
                    onClick={() => dispatch({ type: ActionTypes.NEXT_WEEK })}
                >
                    <FaChevronRight />
                    <span>Next</span>
                </button>
            </p>
        </div>
    );
};
