import { getWeek } from "../../utils/date-wrangler";

export const ActionTypes = Object.freeze({
    'NEXT_WEEK': 1,
    'PREV_WEEK': 2,
    'TODAY': 3,
    'SET_DATE': 4,
});

export const reducer = (state, action) => {
    switch (action.type) {
        case ActionTypes.NEXT_WEEK:
            return getWeek(state.date, 7);
        case ActionTypes.PREV_WEEK:
            return getWeek(state.date, -7);
        case ActionTypes.TODAY:
            return getWeek(new Date());
        case ActionTypes.SET_DATE:
            return getWeek(new Date(action.payload));
        default:
            throw new Error(`Unknown action type: ${action.type}`);
    }
};
