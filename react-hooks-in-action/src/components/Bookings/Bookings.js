import { WeekPicker } from "./WeekPicker";
import { useReducer, useState } from "react";
import { getWeek } from "../../utils/date-wrangler";
import { reducer as weekReducer } from "./weekReducer";
import { BookingsGrid } from "./BookingsGrid";
import { BookingDetails } from "./BookingDetails";

export const Bookings = ({ bookable }) => {
    const [ week, dispatch ] = useReducer(weekReducer, new Date(), getWeek);
    const [ booking, setBooking ] = useState(null);

    return (
        <div className='bookings'>
            <div>
                <WeekPicker date={new Date()}/>

                <BookingsGrid
                    week={week}
                    bookable={bookable}
                    booking={booking}
                    setBooking={setBooking}
                />
            </div>

            <BookingDetails
                booking={booking}
                bookable={booking}
            />
        </div>
    );
};
