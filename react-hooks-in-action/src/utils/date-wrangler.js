const startOfWeek = 1 // 0 - Sunday, 1 - Monday, 6 - Saturday;

export const addDays = (date, daysToAdd) => {
    const clone = new Date(date.getTime());
    clone.setDate(clone.getDate() + daysToAdd);
    return clone;
};

export const getWeek = (forDate, daysOffset = 0) => {
    const date = addDays(forDate, daysOffset);
    const day = (date.getDay() + 7 - startOfWeek) % 7;

    return {
        date,
        start: addDays(date, -day),
        end: addDays(date, 6 - day),
    };
};
