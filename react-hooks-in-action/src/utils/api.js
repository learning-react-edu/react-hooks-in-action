export const getData = async (url) => {
    const response = await fetch(url);
    if (!response.ok) {
        throw Error("There was a problem fetching data.");
    }
    return await response.json();
};
